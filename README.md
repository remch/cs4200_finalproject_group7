Ryan Emch

Source Code README instructions:


The game is implemented in the programming language of python. The file is titled C4_AI.py. To execute the game both numpy and pygame need to be installed. Once everything is correctly installed, simply run the program and the game board will automatically pop up. The game is designed so that it is random on the order of who gets to go first, the AI or yourself. Once the game is up just click a column to drop your game disk. After the game is completed it will immediately close. To play another round you will need to execute the code again.


The C++ implementation's source code is saved in the "4inarow_joel.cpp" text file. The up-to-date version of the game and AI are executable from the "4inarow_joel.exe", which will open a command prompt window in which the game is played by typing in a number between 1 and 7, representing the column in which you want to drop your piece. The AI agent will place their piece after your first move and every second following move. Once the game completes, a choice will appear to either play another game, or to end the game, closing the command prompt window.

The "Final Project Report.pdf" contains the final version of the project report with documentation on both the Python and C++ implementations.

References

Estes, M. (2013) “C++ Connect 4 Command Prompt” [Source code]. 
https://gist.github.com/MichaelEstes/7836988