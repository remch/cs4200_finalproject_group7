#include <iostream>
#include <string>

int roundNumber = 0;

struct playerInfo
{
	char playerName[81];
	char playerID;
};

int PlayerDrop( char board[9][10], playerInfo activePlayer );
int AIDrop( char board[9][10], playerInfo activePlayer, playerInfo passivePlayer);
int RadialHeuristic( char board[9][10], playerInfo activePlayer);
void CheckBellow ( char board[9][10], playerInfo activePlayer, int dropChoice );
void DisplayBoard ( char board[9][10] );
int CheckFour ( char board[9][10], playerInfo activePlayer );
int FullBoard( char board[9][10] );
void PlayerWin ( playerInfo activePlayer );
int restart ( char board[9][10] );

int main()
{
	playerInfo playerOne, playerTwo;
	char board[9][10]; 
	int trueWidth = 7; 
	int trueLength = 6; 
	int dropChoice, win, full, again;

	std::cout << "Let's Play Connect 4" << std::endl << std::endl;
	std::cout << "Player One please enter your name: ";
	std::cin  >> playerOne.playerName;
	playerOne.playerID = 'X';
	playerTwo.playerID = 'O';
	
	full = 0;
	win = 0;
	again = 0;
	DisplayBoard( board );
	do
	{
		dropChoice = PlayerDrop( board, playerOne );
		CheckBellow( board, playerOne, dropChoice );
		DisplayBoard( board );
		win = CheckFour( board, playerOne );
		if ( win == 1 )
		{
			PlayerWin(playerOne);
			again = restart(board);
			if (again == 2)
			{
				break;
			}
		}
		std::cout << std::endl << "AI's Turn" << std::endl;
		dropChoice = AIDrop( board, playerTwo, playerOne );
		CheckBellow( board, playerTwo, dropChoice );
		roundNumber++;
		DisplayBoard( board );
		win = CheckFour( board, playerTwo );
		if ( win == 1 )
		{
			PlayerWin(playerTwo);
			again = restart(board);
			if (again == 2)
			{
				break;
			}
		}
		full = FullBoard( board );
		if ( full == 7 )
		{
			std::cout << "The board is full, it is a draw!" << std::endl;
			again = restart(board);
		}

	}while ( again != 2 );

	

return 0;
}

int PlayerDrop( char board[9][10], playerInfo activePlayer )
{
	int dropChoice;
	do
	{
		std::cout << activePlayer.playerName << "'s Turn ";
		std::cout << "Please enter a number between 1 and 7: ";
		std::cin  >> dropChoice;

		while ( board[1][dropChoice] == 'X' || board[1][dropChoice] == 'O' )
		{
			std::cout << "That row is full, please enter a new row: ";
			std::cin  >> dropChoice;
		}

	}while ( dropChoice < 1 || dropChoice > 7 );

return dropChoice;
}

int AIDrop (char board[9][10], playerInfo activePlayer, playerInfo passivePlayer) {
	char copyBoard[9][10];
	int maxConsecNew;
	int maxConsecPre = RadialHeuristic(board, activePlayer);
	int bestDrop = 4;
	int winDrop = -1;
	int counterDrop = -1;
	
	for (int i = 1; i <= 7; i++) {
		for(int j = 0; j < 9; j++) {
			for(int k = 0; k < 10; k++) {
				copyBoard[j][k] = board[j][k];
			}
		}
		CheckBellow(copyBoard, activePlayer, i);
		maxConsecNew = RadialHeuristic(copyBoard, activePlayer);
		if (maxConsecNew > maxConsecPre) { bestDrop = i; }
		if (CheckFour(copyBoard, activePlayer) == 1) { winDrop = i; }
	}
	for (int i = 1; i <= 7; i++) {
		for(int j = 0; j < 9; j++) {
			for(int k = 0; k < 10; k++) {
				copyBoard[j][k] = board[j][k];
			}
		}
		CheckBellow(copyBoard, passivePlayer, i);
		if (CheckFour(copyBoard, passivePlayer) == 1) { counterDrop = i; }
	}
	if (winDrop > 0) { return winDrop; }
	if (counterDrop > 0) { return counterDrop; }
	if (roundNumber < 1) { return 4; }
	return bestDrop;
}

int RadialHeuristic (char board[9][10], playerInfo activePlayer) {
	char XO = activePlayer.playerID;
	int tempConsec = 0;
	int maxConsecPre = 0;
	for(int i = 8; i >= 1; --i) {
			
		for(int ix = 9; ix >= 1; --ix) {
				
			if(board[i][ix] == XO) {
				tempConsec++;
				if(board[i-1][ix-1] == XO) {
					tempConsec++;
					if(board[i-2][ix-2] == XO) {
						tempConsec++;
						if(board[i-3][ix-3] == XO) {
							tempConsec++;
						}
					}
				}
			
	
				if(board[i][ix-1] == XO) {
					tempConsec++;
					if(board[i][ix-2] == XO) {
						tempConsec++;
						if(board[i][ix-3] == XO) {
							tempConsec++;
						}
					}
				}
				
				if(board[i-1][ix] == XO) {
					tempConsec++;
					if(board[i-2][ix] == XO) {
						tempConsec++;
						if(board[i-3][ix] == XO) {
							tempConsec++;
						}
					}
				}
	
				if(board[i-1][ix+1] == XO) {
					tempConsec++;
					if(board[i-2][ix+2] == XO) {
						tempConsec++;
						if(board[i-3][ix+3] == XO) {
							tempConsec++;
						}
					}
				}			
			
				if(board[i][ix+1] == XO) {
					tempConsec++;
					if(board[i][ix+2] == XO) {
						tempConsec++;
						if(board[i][ix+3] == XO) {
							tempConsec++;
						}
					}
				}
				
			}
		}
		if (tempConsec > maxConsecPre) { maxConsecPre = tempConsec; }
		tempConsec = 0;
	}
	return maxConsecPre;
}

void CheckBellow ( char board[9][10], playerInfo activePlayer, int dropChoice )
{
	int length, turn;
	length = 6;
	turn = 0;

	do 
	{
		if ( board[length][dropChoice] != 'X' && board[length][dropChoice] != 'O' )
		{
			board[length][dropChoice] = activePlayer.playerID;
			turn = 1;
		}
		else
		--length;
	}while (  turn != 1 );


}

void DisplayBoard ( char board[9][10] )
{
	int rows = 6, columns = 7, i, ix;
	
	for(i = 1; i <= rows; i++)
	{
		std::cout << "|";
		for(ix = 1; ix <= columns; ix++)
		{
			if(board[i][ix] != 'X' && board[i][ix] != 'O')
				board[i][ix] = '*';

			std::cout << board[i][ix];
			
		}

		std::cout << "|" << std::endl;
	}

}

int CheckFour ( char board[9][10], playerInfo activePlayer )
{
	char XO;
	int win;
	
	XO = activePlayer.playerID;
	win = 0;

	for( int i = 8; i >= 1; --i )
	{
		
		for( int ix = 9; ix >= 1; --ix )
		{
			
			if( board[i][ix] == XO     &&
				board[i-1][ix-1] == XO &&
				board[i-2][ix-2] == XO &&
				board[i-3][ix-3] == XO )
			{
				win = 1;
			}
			

			if( board[i][ix] == XO   &&
				board[i][ix-1] == XO &&
				board[i][ix-2] == XO &&
				board[i][ix-3] == XO )
			{
				win = 1;
			}
					
			if( board[i][ix] == XO   &&
				board[i-1][ix] == XO &&
				board[i-2][ix] == XO &&
				board[i-3][ix] == XO )
			{	
				win = 1;
			}
					
			if( board[i][ix] == XO     &&
				board[i-1][ix+1] == XO &&
				board[i-2][ix+2] == XO &&
				board[i-3][ix+3] == XO )
			{
				win = 1;
			}
						
			if ( board[i][ix] == XO   &&
				 board[i][ix+1] == XO &&
				 board[i][ix+2] == XO &&
				 board[i][ix+3] == XO )
			{
				win = 1;
			}
		}
		
	}

return win;
}

int FullBoard( char board[9][10] )
{
	int full;
	full = 0;
	for ( int i = 1; i <= 7; ++i )
	{
		if ( board[1][i] != '*' )
			++full;
	}

return full;
}

void PlayerWin ( playerInfo activePlayer )
{
	if (activePlayer.playerID == 'O') { std::cout << std::endl << "AI Connected Four, AI Wins!" << std::endl; }
	std::cout << std::endl << activePlayer.playerName << " Connected Four, You Win!" << std::endl;
}

int restart ( char board[9][10] )
{
	int restart;

	std::cout << "Would you like to restart? Yes(1) No(2): ";
	std::cin  >> restart;
	if ( restart == 1 )
	{
		roundNumber = 0;
		for(int i = 1; i <= 6; i++)
		{
			for(int ix = 1; ix <= 7; ix++)
			{
				board[i][ix] = '*';
			}
		}
	}
	else
		std::cout << "Goodbye!" << std::endl;
return restart;
}