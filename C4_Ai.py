import numpy as np
import random
import pygame
import sys
import math

BLUE = (0,0,255)
BLACK = (0,0,0)
AQUA = (0,255,255)
DARKORCHID = (154,50,205)

ROW_NUM = 6
COLUMN_NUM = 7

PLAYER = 0
AI = 1

EMPTY = 0
PLAYER_disk = 1
AI_disk = 2

WINDOW_LENGTH = 4

def make_board():
	board = np.zeros((ROW_NUM,COLUMN_NUM))
	return board

def place_disk(board, row, col, disk):
	board[row][col] = disk

def apropiate_location(board, col):
	return board[ROW_NUM-1][col] == 0

def open_next_row(board, col):
	for r in range(ROW_NUM):
		if board[r][col] == 0:
			return r

def display_board(board):
	print(np.flip(board, 0))

def final_move(board, disk):
	# Check horizontal locations for victory
	for c in range(COLUMN_NUM-3):
		for r in range(ROW_NUM):
			if board[r][c] == disk and board[r][c+1] == disk and board[r][c+2] == disk and board[r][c+3] == disk:
				return True

	# Check vertical locations for victory
	for c in range(COLUMN_NUM):
		for r in range(ROW_NUM-3):
			if board[r][c] == disk and board[r+1][c] == disk and board[r+2][c] == disk and board[r+3][c] == disk:
				return True

	# Check positive diaganols
	for c in range(COLUMN_NUM-3):
		for r in range(ROW_NUM-3):
			if board[r][c] == disk and board[r+1][c+1] == disk and board[r+2][c+2] == disk and board[r+3][c+3] == disk:
				return True

	# Check negative diaganols
	for c in range(COLUMN_NUM-3):
		for r in range(3, ROW_NUM):
			if board[r][c] == disk and board[r-1][c+1] == disk and board[r-2][c+2] == disk and board[r-3][c+3] == disk:
				return True

def check_window(window, disk):
	score = 0
	opp_disk = PLAYER_disk
	if disk == PLAYER_disk:
		opp_disk = AI_disk

	if window.count(disk) == 4:
		score += 100
	elif window.count(disk) == 3 and window.count(EMPTY) == 1:
		score += 5
	elif window.count(disk) == 2 and window.count(EMPTY) == 2:
		score += 2

	if window.count(opp_disk) == 3 and window.count(EMPTY) == 1:
		score -= 4

	return score

def position_check(board, disk):
	score = 0

	## Score center column
	middle_array = [int(i) for i in list(board[:, COLUMN_NUM//2])]
	center_count = middle_array.count(disk)
	score += center_count * 3

	## Score Horizontal
	for r in range(ROW_NUM):
		row_array = [int(i) for i in list(board[r,:])]
		for c in range(COLUMN_NUM-3):
			window = row_array[c:c+WINDOW_LENGTH]
			score += check_window(window, disk)

	## Score Vertical
	for c in range(COLUMN_NUM):
		col_array = [int(i) for i in list(board[:,c])]
		for r in range(ROW_NUM-3):
			window = col_array[r:r+WINDOW_LENGTH]
			score += check_window(window, disk)

	## Score diagonal
	for r in range(ROW_NUM-3):
		for c in range(COLUMN_NUM-3):
			window = [board[r+i][c+i] for i in range(WINDOW_LENGTH)]
			score += check_window(window, disk)

	for r in range(ROW_NUM-3):
		for c in range(COLUMN_NUM-3):
			window = [board[r+3-i][c+i] for i in range(WINDOW_LENGTH)]
			score += check_window(window, disk)

	return score

def final_node(board):
	return final_move(board, PLAYER_disk) or final_move(board, AI_disk) or len(check_valid_locations(board)) == 0

def min_and_max(board, depth, alpha, beta, maximizingPlayer):
	valid_locations = check_valid_locations(board)
	is_last = final_node(board)
	if depth == 0 or is_last:
		if is_last:
			if final_move(board, AI_disk):
				return (None, 100000000000000)
			elif final_move(board, PLAYER_disk):
				return (None, -10000000000000)
			else: # Game over, no more moves
				return (None, 0)
		else: # Depth = 0
			return (None, position_check(board, AI_disk))
	if maximizingPlayer:
		value = -math.inf
		column = random.choice(valid_locations)
		for col in valid_locations:
			row = open_next_row(board, col)
			b_copy = board.copy()
			place_disk(b_copy, row, col, AI_disk)
			new_score = min_and_max(b_copy, depth-1, alpha, beta, False)[1]
			if new_score > value:
				value = new_score
				column = col
			alpha = max(alpha, value)
			if alpha >= beta:
				break
		return column, value

	else: # Minimizing player
		value = math.inf
		column = random.choice(valid_locations)
		for col in valid_locations:
			row = open_next_row(board, col)
			b_copy = board.copy()
			place_disk(b_copy, row, col, PLAYER_disk)
			new_score = min_and_max(b_copy, depth-1, alpha, beta, True)[1]
			if new_score < value:
				value = new_score
				column = col
			beta = min(beta, value)
			if alpha >= beta:
				break
		return column, value

def check_valid_locations(board):
	valid_locations = []
	for col in range(COLUMN_NUM):
		if apropiate_location(board, col):
			valid_locations.append(col)
	return valid_locations

def optimal_move(board, disk):

	valid_locations = check_valid_locations(board)
	optimal_score = -10000
	optimal_col = random.choice(valid_locations)
	for col in valid_locations:
		row = open_next_row(board, col)
		temp_board = board.copy()
		place_disk(temp_board, row, col, disk)
		score = position_check(temp_board, disk)
		if score > optimal_score:
			optimal_score = score
			optimal_col = col

	return optimal_col

def create_board(board):
	for c in range(COLUMN_NUM):
		for r in range(ROW_NUM):
			pygame.draw.rect(screen, BLUE, (c*SIZEOFSQUARE, r*SIZEOFSQUARE+SIZEOFSQUARE, SIZEOFSQUARE, SIZEOFSQUARE))
			pygame.draw.circle(screen, BLACK, (int(c*SIZEOFSQUARE+SIZEOFSQUARE/2), int(r*SIZEOFSQUARE+SIZEOFSQUARE+SIZEOFSQUARE/2)), RADIUS)

	for c in range(COLUMN_NUM):
		for r in range(ROW_NUM):		
			if board[r][c] == PLAYER_disk:
				pygame.draw.circle(screen, AQUA, (int(c*SIZEOFSQUARE+SIZEOFSQUARE/2), height-int(r*SIZEOFSQUARE+SIZEOFSQUARE/2)), RADIUS)
			elif board[r][c] == AI_disk: 
				pygame.draw.circle(screen, DARKORCHID, (int(c*SIZEOFSQUARE+SIZEOFSQUARE/2), height-int(r*SIZEOFSQUARE+SIZEOFSQUARE/2)), RADIUS)
	pygame.display.update()

board = make_board()
display_board(board)
game_finished = False

pygame.init()

SIZEOFSQUARE = 100

width = COLUMN_NUM * SIZEOFSQUARE
height = (ROW_NUM+1) * SIZEOFSQUARE

size = (width, height)

RADIUS = int(SIZEOFSQUARE/2 - 5)

screen = pygame.display.set_mode(size)
create_board(board)
pygame.display.update()

myfont = pygame.font.SysFont("monospace", 75)

turn = random.randint(PLAYER, AI)

while not game_finished:

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()

		if event.type == pygame.MOUSEMOTION:
			pygame.draw.rect(screen, BLACK, (0,0, width, SIZEOFSQUARE))
			posx = event.pos[0]
			if turn == PLAYER:
				pygame.draw.circle(screen, AQUA, (posx, int(SIZEOFSQUARE/2)), RADIUS)

		pygame.display.update()

		if event.type == pygame.MOUSEBUTTONDOWN:
			pygame.draw.rect(screen, BLACK, (0,0, width, SIZEOFSQUARE))
			#print(current)
			#Player 1 Input
			if turn == PLAYER:
				posx = event.pos[0]
				col = int(math.floor(posx/SIZEOFSQUARE))

				if apropiate_location(board, col):
					row = open_next_row(board, col)
					place_disk(board, row, col, PLAYER_disk)

					if final_move(board, PLAYER_disk):
						label = myfont.render("Player 1 wins!!", 1, AQUA)
						screen.blit(label, (40,10))
						game_finished = True

					turn += 1
					turn = turn % 2

					display_board(board)
					create_board(board)


	# # Player 2 Input
	if turn == AI and not game_finished:				

		#col = random.randint(0, COLUMN_NUM-1)
		#col = optimal_move(board, AI_disk)
		col, min_and_max_score = min_and_max(board, 5, -math.inf, math.inf, True)

		if apropiate_location(board, col):
			#pygame.time.wait(500)
			row = open_next_row(board, col)
			place_disk(board, row, col, AI_disk)

			if final_move(board, AI_disk):
				label = myfont.render("Player 2 wins!!", 1, DARKORCHID)
				screen.blit(label, (40,10))
				game_finished = True

			display_board(board)
			create_board(board)

			turn += 1
			turn = turn % 2

	if game_finished:
		pygame.time.wait(3000)