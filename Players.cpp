#include<iostream>
#include<string>



class PlayerInformation
{
    public:
    char playername[10];
    char playerID;
};




int main()
{
    PlayerInformation FirstPlayer, SecondPlayer;
    char board[7][6];
    int dropSpot, win, full, again;


    std::cout << "What is player one's name? " << std::endl;
    std::cin >> FirstPlayer.playername;
    std::cout << "\n";

    std::cout << "Player one will be assigned 'X' " << std::endl;
    FirstPlayer.playerID = 'X';
    std::cout << "\n";

    std::cout << "What is the second players name? " << std::endl;
    std::cin >> SecondPlayer.playername;
    std::cout << "\n";


    std::cout<< "Player two will be assigned 'O' " << std::endl;
    SecondPlayer.playerID = 'O';
    std::cout << "\n";


    std::cout << "The two players are: " << std::endl;
    std::cout << "\n";

    std::cout << FirstPlayer.playername << std::endl;
    std::cout << "\n";
    std::cout << SecondPlayer.playername << std::endl;
    std::cout << "\n";


    return 0;
}




